FROM sutty/monit:latest
MAINTAINER "f <f@sutty.nl>"

# Install postfix and certificates
RUN apk add --no-cache postfix ca-certificates
# Generate params and remove packages
RUN install -dm 2750 -o root -g root /etc/ssl/private \
  && apk add --no-cache gnutls-utils \
  && certtool --generate-dh-params --outfile=/etc/ssl/private/2048.dh --bits=2048 \
  && certtool --generate-dh-params --outfile=/etc/ssl/private/512.dh --bits=512 \
  && apk del gnutls-utils
# Configure postfix
RUN postconf -e alias_maps='lmdb:/etc/postfix/aliases' \
  && postconf -e transport_maps='lmdb:/etc/postfix/transport' \
  && postconf -e recipient_delimiter='+' \
  && postconf -e sendmail_path='/usr/sbin/sendmail' \
  && postconf -e newaliases_path='/usr/bin/newaliases' \
  && postconf -e mailq_path='/usr/bin/mailq' \
  && postconf -e setgid_group='postdrop' \
  && postconf -e manpage_directory='/usr/share/man' \
  && postconf -e sample_directory='/etc/postfix/sample' \
  && postconf -e readme_directory='/usr/share/doc/postfix' \
  && postconf -e html_directory='no' \
  && postconf -e soft_bounce='no' \
  && postconf -e mydestination='/etc/postfix/maps/domains.cf' \
  && postconf -e inet_interfaces='all' \
  && postconf -e local_recipient_maps='unix:passwd.byname $alias_maps' \
  && postconf -e mynetworks_style='subnet' \
  && postconf -e home_mailbox='Maildir/' \
  && postconf -e message_size_limit=20480000 \
  && postconf -e inet_protocols='all' \
  && postconf -e disable_vrfy_command=yes \
  && postconf -e smtpd_helo_required=yes \
  && postconf -e smtpd_helo_restrictions='permit_mynetworks,permit_sasl_authenticated,reject_non_fqdn_helo_hostname,reject_invalid_helo_hostname,reject_unknown_helo_hostname,permit' \
  && postconf -e smtpd_recipient_restrictions='reject_non_fqdn_recipient,reject_unknown_recipient_domain,permit_mynetworks,permit_sasl_authenticated,reject_unauth_destination,reject_non_fqdn_sender,reject_unlisted_recipient' \
  && postconf -e smtpd_sender_restrictions='permit_mynetworks,permit_sasl_authenticated,reject_non_fqdn_sender,reject_unknown_sender_domain,permit' \
  && postconf -e smtpd_data_restrictions='reject_unauth_pipelining' \
  && postconf -e smtpd_client_restrictions='permit_mynetworks,permit_sasl_authenticated' \
  && postconf -e smtpd_relay_restrictions='permit_mynetworks,permit_sasl_authenticated,reject_unauth_destination' \
  && postconf -e smtpd_use_tls='yes' \
  && postconf -e smtpd_tls_auth_only='yes' \
  && postconf -e smtp_use_tls='yes' \
  && postconf -e smtp_tls_note_starttls_offer='yes' \
  && postconf -e smtpd_tls_CApath='/etc/ssl/certs' \
  && postconf -e tls_random_source='dev:/dev/urandom' \
  && postconf -e smtpd_tls_dh1024_param_file='/etc/ssl/private/2048.dh' \
  && postconf -e smtpd_tls_dh512_param_file='/etc/ssl/private/512.dh' \
  && postconf -e tls_preempt_cipherlist='yes' \
  && postconf -e smtpd_tls_security_level='may' \
  && postconf -e smtpd_tls_eecdh_grade='strong' \
  && postconf -e smtpd_tls_mandatory_ciphers='high' \
  && postconf -e smtpd_tls_ciphers='medium' \
  && postconf -e smtpd_tls_exclude_ciphers='aNULL,MD5,DES,3DES,DES-CBC3-SHA,RC4-SHA,AES256-SHA,AES128-SHA,eNULL,EXPORT,RC4,PSK,aECDH,EDH-DSS-DES-CBC3-SHA,EDH-RSA-DES-CDC3-SHA,KRB5-DE5,CBC3-SHA' \
  && postconf -e smtpd_tls_mandatory_protocols='TLSv1' \
  && postconf -e smtp_tls_ciphers='$smtpd_tls_ciphers' \
  && postconf -e smtp_tls_mandatory_ciphers='$smtpd_tls_mandatory_ciphers' \
  && postconf -e smtp_tls_protocols='!SSLv2,!SSLv3,TLSv1' \
  && postconf -e smtpd_tls_loglevel='0' \
  && postconf -e smtpd_tls_received_header='yes' \
  && postconf -e smtpd_tls_session_cache_timeout='3600s' \
  && postconf -e smtp_destination_concurrency_limit=2 \
  && postconf -e smtp_destination_rate_delay=1s \
  && postconf -e smtp_extra_recipient_limit=10 \
  && postconf -e append_dot_mydomain=yes \
  && postconf -e masquerade_domains='$mydomain' \
  && postconf -e non_smtpd_milters=inet:opendkim:8891 \
  && postconf -e milter_default_action=accept \
  && postconf -e milter_protocol=6 \
  && postconf -e virtual_alias_maps=lmdb:/etc/postfix/maps/virtual \
  && postconf -e sender_canonical_maps=tcp:postsrsd:10001 \
  && postconf -e sender_canonical_classes=envelope_sender \
  && postconf -e recipient_canonical_maps=tcp:postsrsd:10002 \
  && postconf -e recipient_canonical_classes=envelope_recipient,header_recipient

RUN newaliases
RUN postmap /etc/postfix/transport

# Enable service
COPY ./monit.conf /etc/monit.d/postfix.conf
COPY ./postfix.sh /usr/local/bin/postfix
RUN chmod 755 /usr/local/bin/postfix

VOLUME "/etc/postfix/maps"

# Port
EXPOSE 25
