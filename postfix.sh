#!/bin/sh

# Reconfigure postfix according to environment
postconf -e myhostname="${SUTTY}"
postconf -e mydomain="${SUTTY}"
postconf -e smtpd_tls_key_file="/etc/letsencrypt/live/${SUTTY}/privkey.pem"
postconf -e smtpd_tls_cert_file="/etc/letsencrypt/live/${SUTTY}/fullchain.pem"
postconf -e smtpd_milters="inet:rspamd.${DELEGATE}:11332,inet:opendkim:8891"

exec /usr/sbin/postfix start
